# iwd-eap

You should definitely first look into the all configuration options mentioned here: https://iwd.wiki.kernel.org/networkconfigurationsettings.

Here is a simple configuration for EAP TLS. You should provide/generate the certificate and the key and move to a directory accessible to IWD, e.g. `/var/lib/iwd`.

```
#/var/www/iwd/your_essid_name.8021x
[Security]
EAP-Method=TLS
EAP-Identity=john.doe
EAP-TLS-ClientCert=/var/lib/iwd/filename.crt
EAP-TLS-ClientKey=/var/lib/iwd/filename.key
EAP-TLS-ClientKeyPassphrase=mysecretpassword

[Settings]
AutoConnect=true
```